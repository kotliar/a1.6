import {RoutesConfig} from './config/routes.config';

angular.module('app.config')
    .config(RoutesConfig);
