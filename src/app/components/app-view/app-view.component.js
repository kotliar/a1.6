class AppViewController {
    constructor() {
        'ngInject';
    }

    $onInit() {}

}

export const AppViewComponent = {
    templateUrl: './views/components/app-view/app-view.component.html',
    controller: AppViewController,
    controllerAs: 'vm',
    bindings: {}
}
