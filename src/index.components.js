import {AppViewComponent} from './app/components/app-view/app-view.component';

angular.module('app.components')
    .component('appView', AppViewComponent);
