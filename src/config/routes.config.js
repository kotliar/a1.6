export function RoutesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
	'ngInject';

    $locationProvider.html5Mode(true);

	let getView = (viewName) => {
		return `./views/app/pages/${viewName}/${viewName}.page.html`;
	};

	$urlRouterProvider.otherwise('/');

	// $stateProvider
    //     .state('login', {
	// 		url: '/',
	// 		views: {
    //             'header': {},
	// 			'main@': {
	// 				templateUrl: getView('login')
	// 			},
    //             'footer':{
    //                 templateUrl: getView('footer')
    //             }
	// 		},
    //         params:{
    //             data: 'login'
    //         }
	// 	});
}
