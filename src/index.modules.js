angular.module('app', [
  'app.run',
  'app.components',
  'app.routes',
  'app.config',
  'app.partials'
])

angular.module('app.run', [])
angular.module('app.routes', [])
angular.module('app.config', [])
angular.module('app.components', [
  'ui.router', 'ngAnimate', 'ngSanitize','schemaForm'
])
