var gulp = require('gulp');
var concat = require('gulp-concat');
var gulpif = require('gulp-if');
var eslint = require('gulp-eslint');
var uglify = require('gulp-uglify');
var ngAnnotate = require('gulp-ng-annotate');
var ngHtml2Js = require('gulp-ng-html2js');
var minifyHtml = require('gulp-htmlmin');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();
var webpack = require('webpack-stream');
var webpackConfig = require('./webpack.config.js');

var paths = {
    scss:['./src/**/**/*.scss','./node_modules/bootstrap/dist/css/bootstrap.css'],
    vendor: [
        './node_modules/angular/angular.js',
        './node_modules/angular-sanitize/angular-sanitize.js',
        './node_modules/angular-ui-router/release/angular-ui-router.js',
        './node_modules/angular-animate/angular-animate.js',
        './node_modules/tv4/tv4.js',
        './node_modules/objectpath/lib/ObjectPath.js',
        './node_modules/angular-schema-form/dist/schema-form.min.js',
        './node_modules/angular-schema-form/dist/bootstrap-decorator.min.js'
    ]
};

gulp.task('vendor', function() {
    return gulp.src(paths.vendor)
		.pipe(concat('vendor.js', {sourcesContent: true}))
		.pipe(gulp.dest('public/js/'));
});

gulp.task('sass', function () {
 return gulp.src(paths.scss)
  	.pipe(sourcemaps.init())
  	.pipe(sass().on('error', sass.logError))
  	.pipe(concat('app.css'))
  	.pipe(sourcemaps.write())
  	.pipe(gulp.dest('./public/css'));
});

gulp.task('scripts', function() {
	return gulp.src(['./src/index.main.js', './src/**/*.*.js'])
        // .pipe(eslint())
        // .pipe(eslint.format())
        .pipe(sourcemaps.init())
        .pipe(webpack(webpackConfig))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(sourcemaps.write())
    	.pipe(gulp.dest('public/js'));
});

var defaultOptions = {
    moduleName: 'app.partials',
    prefix: './views/'
};

gulp.task('ng-html-js', function(){
    return gulp.src(['./src/app/**/**/*.html'])
            .pipe(minifyHtml({
                empty: true,
                spare: true,
                quotes: true
            }))
            .pipe(ngHtml2Js(defaultOptions))
            .pipe(concat('partials.js'))
            .pipe(uglify())
            .pipe(gulp.dest('public/js/'));
});

gulp.task('copyIndex', function () {
    return gulp.src(['./src/index.html']).pipe(gulp.dest('public/'));
});

gulp.task('default', ['vendor', 'sass', 'scripts', 'copyIndex','ng-html-js']);

gulp.task('watch', function() {
    browserSync.init({
        server: "./public"
    });
	gulp.watch(paths.scss, ['sass']).on('change', function(evt) {
        browserSync.reload();
    });
	gulp.watch('./src/**/*.js', ['scripts']).on('change', function(evt) {
        browserSync.reload();
    });
	gulp.watch('./src/app/**/**/*.html', ['ng-html-js']).on('change', function(evt) {
        browserSync.reload();
    });
});
